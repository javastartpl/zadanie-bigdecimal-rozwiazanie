import java.math.BigDecimal;

public class Sale {

    private String name;
    private BigDecimal brutto;
    private int vatPercent;

    public Sale(String name, BigDecimal brutto, int vatPercent) {
        this.name = name;
        this.brutto = brutto;
        this.vatPercent = vatPercent;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getBrutto() {
        return brutto;
    }

    public int getVatPercent() {
        return vatPercent;
    }
}
