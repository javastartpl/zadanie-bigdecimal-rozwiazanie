import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class SalesCalculator {

    public void run() {
        SalesReader salesReader = new SalesReader();

        try {
            List<Sale> sales = salesReader.readFromFile();

            BigDecimal sumBrutto = BigDecimal.ZERO;
            BigDecimal sumNetto = BigDecimal.ZERO;

            for (Sale sale : sales) {
                BigDecimal brutto = sale.getBrutto();

                int vatPercent = sale.getVatPercent();
                BigDecimal vatDivider = BigDecimal.ONE
                        .add(BigDecimal.valueOf(vatPercent).divide(BigDecimal.valueOf(100), 2, RoundingMode.UNNECESSARY));

                BigDecimal netto = brutto.divide(vatDivider, 2, RoundingMode.HALF_UP);
                sumBrutto = sumBrutto.add(brutto);
                sumNetto = sumNetto.add(netto);
            }

            System.out.println("Suma brutto: " + sumBrutto);
            System.out.println("Suma netto: " + sumNetto);
            System.out.println("Suma vat: " + (sumBrutto.subtract(sumNetto)));



        } catch (IOException e) {
            System.out.println("Nie udało się wczytać danych. Powód: " + e.getMessage());
        }

    }
}
