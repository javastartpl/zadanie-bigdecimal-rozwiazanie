import java.io.*;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class SalesReader {


    List<Sale> readFromFile() throws IOException {

        File file = new File("sales.csv");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line = null;

        List<Sale> sales = new LinkedList<>();

        while ((line = bufferedReader.readLine()) != null) {
            String[] split = line.split(";");

            String name = split[0];
            BigDecimal brutto = new BigDecimal(split[1]);
            int vatPercent = Integer.parseInt(split[2]);

            Sale sale = new Sale(name, brutto, vatPercent);
            sales.add(sale);
        }

        return sales;

    }


}
